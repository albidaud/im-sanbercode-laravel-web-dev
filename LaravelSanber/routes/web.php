<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;
use App\Http\Controllers\PeranController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'homePage']);

// Route::get('/register', function () {
//     return view('halaman.register');
// });

Route::get('/register', [AuthController::class, 'registerPage']);

Route::post('/welcome', [AuthController::class, 'welcomePage']);

// Route Tugas 13 - Laravel Template
Route::get('/table', function() {
    return view('layouts.table');
});

Route::get('/data-tables', function() {
    return view('layouts.table_data');
});

// Route Tugas 15 - Laravel CRUD

// CREATE
// Route mengarahkan ke halaman tambah/add pemain film baru
Route::get('/cast/create', [CastController::class, 'create']);

// Route menyimpan inputan dari form ke dalam database tugas_14, tabel cast
Route::post('/cast', [CastController::class, 'store']);

// READ
// Route menampilkan halaman semua data pada table cast
Route::get('/cast', [CastController::class, 'index']);

// Route menampilkan detail data pemain film dengan id tertentu
Route::get('/cast/{cast_id}', [CastController::class, 'show']);

// UPDATE
// Route menampilkan form untuk edit pemain film dengan id tertentu
Route::get('/cast/{cast_id}/edit', [CastController::class, 'edit']);

// Route menyimpan perubahan data pemain film (update) untuk id tertentu
Route::put('/cast/{cast_id}', [CastController::class, 'update']);

// DELETE
// Route menghapus data pemain film dengan id tertentu
Route::delete('/cast/{cast_id}', [CastController::class, 'destroy']);

// CRUD Table Film
Route::resource('peran', PeranController::class);