<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cast;
use App\Models\Peran;
use File;

class PeranController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $peran = Peran::all();
        return view('peran.tampil', ['peran'=>$peran]);  
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cast = Cast::all();
        return view('peran.tambah', ['cast' => $cast]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'content' => 'required',
            'cast_id' => 'required',
            'image' => 'required|mimes:png,jpg,jpeg|max:2048',
    
            ]);

            // Konversi nama gambar
            $imageName = time().'.'.$request->image->extension();

            //file gambar masuk ke folder public
            $request->image->move(public_path('image'), $imageName);

            // Insert data ke database
            $peran = new Peran;

            $peran->title = $request->title;
            $peran->content = $request->content;
            $peran->cast_id = $request->cast_id;
            $peran->image = $imageName;
    
            $peran->save();

            // Kembali ke halaman /peran
            return redirect('/peran');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $peran = Peran::find($id);

        return view('peran.detail', ['peran'=> $peran]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cast = Cast::all();
        $peran = Peran::find($id);

        return view('peran.edit', ['peran'=> $peran, 'cast'=> $cast]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required',
            'content' => 'required',
            'cast_id' => 'required',
            'image' => 'mimes:png,jpg,jpeg|max:2048',
    
            ]);

            $peran  = Peran::find($id);

            $peran->title = $request->title;
            $peran->content = $request->content;
            $peran->cast_id = $request->cast_id;
            

            if ($request->has('image')) {
                $path = 'image/';
                File::delete($path. $peran->image);

                // Konversi nama gambar
                $imageName = time().'.'.$request->image->extension();

                //file gambar masuk ke folder public
                $request->image->move(public_path('image'), $imageName);

                $peran->image = $imageName;
            }
            $peran->save();

            return redirect('/peran');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $peran = Peran::find($id);

        $path = 'image/';
        File::delete($path. $peran->image);

        $peran->delete();

        return redirect('/peran');
    }
}
