<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function create()
    {
        return view('cast.create_cast');
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ],
        [
            'nama.required' => 'Silakan masukkan nama terlebih dahulu',
            'umur.required' => 'Masukkan umur anda',
            'bio.required' => 'Masukkan bio anda',
        ]);

        DB::table('cast')->insert([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio' => $request['bio']
        ]);

        return redirect('/cast');
    }

    public function index()
    {
        $cast = DB::table('cast')->get();

        // dd($cast);

        return view('cast.tampil', ['cast' => $cast]);
    }

    public function show($id)
    {
        $cast2 = DB::table('cast')->find($id);

        return view('cast.detail', ['cast2' => $cast2]);
    }

    public function edit($id)
    {
        $cast2 = DB::table('cast')->find($id);

        return view('cast.edit', ['cast2' => $cast2]);
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ],
        [
            'nama.required' => 'Silakan masukkan nama terlebih dahulu',
            'umur.required' => 'Masukkan umur anda',
            'bio.required' => 'Masukkan bio anda',
        ]);

        DB::table('cast')
            ->where('id', $id)
            ->update(
                [
                    'nama' => $request['nama'],
                    'umur' => $request['umur'],
                    'bio' => $request['bio']
                ]
            );
        return redirect('/cast');
    }

    public function destroy($id)
    {
        DB::table('cast')->where('id', '=', $id)->delete();

        return redirect('/cast');
    }
}
