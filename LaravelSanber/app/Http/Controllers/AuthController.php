<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function registerPage(){
        return view('halaman.register');
    }

    public function welcomePage(Request $request){
        // dd($request->all());
        $namaDepan = $request['Fname'];
        $namaBelakang = $request['Lname'];

        return view('halaman.welcome', ['namaDepan' => $namaDepan, 'namaBelakang' => $namaBelakang]);
    }
}
