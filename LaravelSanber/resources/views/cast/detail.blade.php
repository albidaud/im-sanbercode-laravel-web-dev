@extends('home')

@section('table')
    <strong>Kategori</strong>
@endsection

@section('sub-table')
    Halaman Detail Kategori
@endsection

@section('content')

    <h1>{{$cast2 -> nama }}</h1>
    <h3>{{$cast2 -> umur}}</h3>
    <p>{{$cast2 -> bio}}</p>

    <a href="/cast" class="btn btn-danger btn-sm">Kembali</a>

@endsection