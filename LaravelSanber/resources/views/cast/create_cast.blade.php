@extends('home')

@section('table')
    <strong>Create</strong>
@endsection

@section('sub-table')
    <p class="text-center">Isi form untuk menambahkan aktor baru</p>
@endsection

@section('content')
    <form action="/cast" method="POST">
        @csrf
        <div class="form-group">
            <label>Nama</label>
            <input type="text" class="form-control" name="nama" placeholder="Masukkan nama">
        </div>
        @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
            <label>Umur</label>
            <input type="number" class="form-control" name="umur" placeholder="Masukkan umur">
        </div>
        @error('umur')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
            <label>Bio</label>
            <textarea name="bio" class="form-control" cols="10" rows="10"></textarea>
        </div>
        @error('bio')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <button type="submit" class="btn btn-warning">Kirim</button>
    </form>
@endsection