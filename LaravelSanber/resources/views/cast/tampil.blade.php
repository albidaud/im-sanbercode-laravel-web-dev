@extends('home')

@section('table')
    <strong>Pemain Film</strong>
@endsection

@section('sub-table')
    <p class="text-center">Daftar Pemain Film</p>
@endsection

@section('content')

<a href="/cast/create" class="btn btn-warning btn-sm mb-3">Tambah Cast / Pemain</a>
    <table class="table">
        <thead class="thead-light">
        <tr>
            <th scope="col">#</th>
            <th scope="col">Nama</th>
            <th scope="col">Umur</th>
            <th scope="col">Bio</th>
        </tr>
        </thead>
        <tbody>
            @forelse ($cast as $key => $item)
                <tr>
                    <td>{{$key + 1}}</td>
                    <td>{{$item  -> nama}}</td>
                    <td>{{$item  -> umur}}</td>
                    <td>
                        <form action="/cast/{{$item->id}}" method="POST">
                            @csrf
                            @method('delete')
                            <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                            <a href="/cast/{{$item->id}}/edit" class="btn btn-success btn-sm">Edit Pemain Film</a>

                            <input type="submit" value="Hapus" class="btn btn-danger btn-sm">
                        </form>
                    </td>
                </tr>
            @empty
                <tr>
                    <td>Data Cast / Pemain Film Kosong</td>
                </tr>
            @endforelse
        </tbody>
    </table>
@endsection