@extends('home')

@section('table')
    <strong>Edit</strong>
@endsection

@section('sub-table')
    <p class="text-center">Halaman Edit Kategori</p>
@endsection

@section('content')
    <form action="/cast/{{$cast2->id}}" method="POST">
        @csrf
        @method('put')
        <div class="form-group">
            <label>Nama</label>
            <input type="text" class="form-control" name="nama" value="{{$cast2->nama}}" placeholder="Masukkan nama">
        </div>
        @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
            <label>Umur</label>
            <input type="number" class="form-control" name="umur" value="{{$cast2->umur}}" placeholder="Masukkan umur">
        </div>
        @error('umur')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
            <label>Bio</label>
            <textarea name="bio" class="form-control" cols="10" rows="10">{{$cast2->bio}}</textarea>
        </div>
        @error('bio')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <button type="submit" class="btn btn-warning">Kirim</button>
    </form>
@endsection