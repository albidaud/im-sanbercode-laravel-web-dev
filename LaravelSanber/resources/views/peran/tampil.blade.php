@extends('home')

@section('table')
    <strong>Tampil Peran</strong>
@endsection

@section('sub-table')
    <p class="text-center">Peran</p>
@endsection

@section('content')
    
    <a href="/peran/create" class="btn btn-warning my-3"><strong>+ Tambah Peran</strong></a>

    <div class="row">
        @forelse ($peran as $item)
            <div class="col-4">
                <div class="card" style="width: 60%;">
                    <img src="{{asset('/image/' . $item->image)}}" height="200px class="card-img-top" alt="...">
                    <div class="card-body">
                        <h3>{{$item->title}}</h3>
                        <p class="card-text"> {{ Str::limit($item->content, 50) }}</p>
                        <a href="/peran/{{$item->id}}" class="btn btn-info btn-block">Read more</a>
                        <div class="row mt-3">
                            <div class="col-6">
                                <a href="/peran/{{$item->id}}/edit" class="btn btn-primary btn-block btn-warning">Edit</a> 
                            </div>

                            <div class="col-6">
                                <form action="/peran/{{$item->id}}" method="POST">
                                    @csrf
                                    @method('delete')
                                    <input type="submit" class="btn btn-danger btn-block" value="Hapus">
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @empty
            <h1>Tidak ada daftar peran</h1>
        @endforelse
    </div>

@endsection