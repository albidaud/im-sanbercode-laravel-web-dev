@extends('home')

@section('table')
    <strong>Tampil Peran</strong>
@endsection

@section('sub-table')
    <p class="text-center">Peran</p>
@endsection

@section('content')
    <div class="card" style="width: 60%;">
        <img src="{{asset('/image/' . $peran->image)}}" class="card-img-top" alt="...">
        <div class="card-body">
            <h3>{{$peran->title}}</h3>
            <p class="card-text"> {{$peran->content}}</p>
        </div>
    </div>

    <a href="/peran" class="btn btn-secondary btn-sm">Kembali</a>
@endsection