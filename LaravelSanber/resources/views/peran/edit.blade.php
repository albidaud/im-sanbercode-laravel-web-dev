@extends('home')

@section('table')
    <strong>Edit Peran</strong>
@endsection

@section('sub-table')
    <p class="text-center">Edit deskripsi peran disini</p>
@endsection

@section('content')
    <form action="/peran/{{$peran->id}}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('put')
        <div class="form-group">
            <label>Title</label>
            <input type="text" class="form-control" value="{{$peran->title}}" name="title" placeholder="Masukkan nama">
        </div>
        @error('title')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
            <label>Content</label>
            <textarea name="content" class="form-control" cols="10" rows="10">{{$peran->content}}</textarea>
        </div>
        @error('content')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
            <label>Cast</label>
            <select name="cast_id" class="form-control" id="">
                <option value="">-- Pilih Cast --</option>

                @forelse ($cast as $item)
                @if ($item->id === $peran->cast_id)
                
                <option value="{{$item->id}}" selected>{{$item->name}}</option>
                @else
                <option value="{{$item->id}}">{{$item->name}}</option>
                @endif
                @empty
                    <option value="">Belum ada data ditambahkan</option>
                @endforelse
            </select>
        </div>
        @error('cast_id')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
            <label>Image</label>
            <input type="file" class="form-control" name="image" placeholder="Masukkan gambar">
        </div>
        @error('image')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <button type="submit" class="btn btn-warning">Kirim</button>
    </form>
@endsection