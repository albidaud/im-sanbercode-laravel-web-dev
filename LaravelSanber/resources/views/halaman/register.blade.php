<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Register Page_Albi</title>
  </head>
  <body>
    <h1>Buat Account Baru!</h1>

    <h3>Sign Up Form</h3>

    <!-- FORM -->
    <form action="/welcome" method="post">
      @csrf
      <label><strong>First Name:</strong></label> <br />
      <br />
      <input type="text" name="Fname" /> <br />
      <br />

      <label><strong>Last Name:</strong></label> <br />
      <br />
      <input type="text" name="Lname" /> <br />
      <br />

      <label><strong>Gender:</strong></label> <br />
      <br />
      <input type="radio" name="gender" />Male <br />
      <input type="radio" name="gender" />Female <br />
      <input type="radio" name="gender" />Other <br />
      <br />

      <label><strong>Nationality:</strong></label> <br />
      <br />
      <select name="nationality">
        <option value="">- Select here -</option>
        <option value="">Indonesian</option>
        <option value="">Filipinos</option>
        <option value="">Singaporean</option>
        <option value="">Chinese</option>
        <option value="">American</option>
      </select>
      <br />
      <br />

      <label><strong>Language Spoken:</strong></label> <br />
      <br />
      <input type="checkbox" />Bahasa Indonesia <br />
      <input type="checkbox" />English <br />
      <input type="checkbox" />Other <br />
      <br />
      <br />

      <label><strong>Bio</strong></label> <br />
      <br />
      <textarea cols="30" rows="10"></textarea> <br />
      <br />

      <input type="submit" value="Sign Up" />
    </form>
    <br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
  </body>
</html>
